import { useNavigate } from "@remix-run/react";
import { useContext, useEffect } from "react";
import { AuthContext } from "~/lib/context/AuthProvider";

export default function Home() {
    const { auth, setAuth }: any = useContext(AuthContext);
    const navigate = useNavigate();

    useEffect(() => {
        if (!auth.loggedIn) navigate("/");

    }, [auth, navigate])

    function logout() {
        setAuth({loggedIn: false, userDetails:{}})
    }

    return <div className="flex flex-col items-center justify-center max-w-64 mx-auto h-screen">
        <span className="text-2xl">Logged in Successfully</span>
        <button onClick={logout} className="bg-[#7747ff] w-full mt-3 px-6 py-2 rounded text-white text-sm font-normal">Logout</button>
    </div>
}