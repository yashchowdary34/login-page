import type { MetaFunction } from "@remix-run/node";
import { useNavigate } from "@remix-run/react";
import { useContext, useEffect, useState } from "react";
import { AuthContext } from "~/lib/context/AuthProvider";

export const meta: MetaFunction = () => {
  return [
    { title: "Login App" },
    { name: "description", content: "Welcome to Login App!" },
  ];
};

export default function Index() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { auth, setAuth }: any = useContext(AuthContext);
  const navigate = useNavigate();

  useEffect(() => {
    if (auth.loggedIn) navigate("/home");

  }, [auth, navigate])

  const onSubmit: React.FormEventHandler = (e) => {
    e.preventDefault();

    if (email.trim() === "") {
      alert("Email is required");
    } else
      if (password.trim() === '') {
        alert("Password is required");
      } else
        if (email === "yashchowdary34@gmail.com" && password === "12345678") {
          setAuth({
            loggedIn: true,
            userDetails: { email, password }
          });
        } else {
          alert("Invalid Creds")
        }
  }


  return (
    <div className="font-sans p-4 flex items-center justify-center w-screen h-screen">
      <div className="max-w-md relative flex flex-col p-4 rounded-md text-black bg-white">
        <div className="text-2xl font-bold mb-2 text-[#1e0e4b] text-center">Welcome back to <span className="text-[#7747ff]">Login App</span></div>
        <div className="text-sm font-normal mb-4 text-center text-[#1e0e4b]">Log in to your account</div>
        <form className="flex flex-col gap-3" onSubmit={onSubmit}>
          <div className="block relative">
            <label htmlFor="email" className="block text-gray-600 cursor-text text-sm leading-[140%] font-normal mb-2">Email</label>
            <input type="email" id="email" onChange={e => setEmail(e.target.value)} className="rounded border border-gray-200 text-sm w-full font-normal leading-[18px] text-black tracking-[0px] appearance-none block h-11 m-0 p-[11px] focus:ring-2 ring-offset-2  ring-gray-900 outline-0" />
          </div>
          <div className="block relative">
            <label htmlFor="password" className="block text-gray-600 cursor-text text-sm leading-[140%] font-normal mb-2">Password</label>
            <input type="text" id="password" onChange={e => setPassword(e.target.value)} className="rounded border border-gray-200 text-sm w-full font-normal leading-[18px] text-black tracking-[0px] appearance-none block h-11 m-0 p-[11px] focus:ring-2 ring-offset-2 ring-gray-900 outline-0" />
          </div>
          <button type="submit" className="bg-[#7747ff] w-full mt-3 px-6 py-2 rounded text-white text-sm font-normal">Submit</button>
        </form>
      </div>
    </div>
  );
}
