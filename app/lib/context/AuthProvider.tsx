import React, { useState, createContext, useMemo } from 'react';

export const AuthContext = createContext({});

function AuthProvider({ children }: any) {
    const [auth, setAuth] = useState({
        loggedIn: false,
        userDetails: {}
    });

    const value = useMemo(() => ({ auth, setAuth }), [auth, setAuth]);

    return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>
}

export default AuthProvider;